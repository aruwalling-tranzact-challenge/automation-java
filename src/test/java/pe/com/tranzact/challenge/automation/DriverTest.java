package pe.com.tranzact.challenge.automation;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import pe.com.tranzact.challenge.automation.driver.DriverFactory;

import java.util.stream.Stream;

@DisplayName("Driver Test")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Slf4j
@Tag("webDriver")
public class DriverTest {
    private DriverFactory factory;

    static Stream<Arguments> driverArguments(){
        return Stream.of(Arguments.arguments("chrome", ChromeDriver.class),Arguments.arguments("firefox", FirefoxDriver.class));
    }

    @BeforeAll
    public void setup(){
        log.info("Loading instance of driver manager");
        factory = new DriverFactory();
    }

    @Test
    @Tag("null-wd")
    @DisplayName("Fail when driver name is null")
    public void failWhenDriverNameIsNull(){
        log.info("Fail when driver name is null");
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class,() -> factory.getDriver(null));
        Assertions.assertEquals("No such driver null",exception.getMessage());
    }

    @ParameterizedTest
    @ValueSource(strings = {"","not valid"})
    @Tag("invalid-wd")
    @DisplayName("Fail when the driver name not exist")
    public void failWhenDriverNotExist(String driverName){
        log.info("Fail when the driver name not exist");
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class,() -> factory.getDriver(driverName));
        Assertions.assertEquals(String.format("No such driver %s",driverName),exception.getMessage());
    }

    //TODO: review EnabledIfSystemProperties to add specific test (Edge in windows and Safari in Mac) https://junit.org/junit5/docs/current/user-guide/#writing-tests-conditional-execution-system-properties
    @ParameterizedTest
    @MethodSource("driverArguments")
    @Tag("valid-wd")
    @DisplayName("Get a valid driver instance")
    public void getValidDriverInstance(String driverName, Class clazz){
        log.info("Get a valid driver instance");
        WebDriver driver = factory.getDriver(driverName);
        Assertions.assertTrue(clazz.isInstance(driver));
        driver.quit();
    }
}
