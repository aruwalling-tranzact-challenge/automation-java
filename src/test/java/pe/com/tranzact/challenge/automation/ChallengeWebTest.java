package pe.com.tranzact.challenge.automation;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.openqa.selenium.WebDriver;
import pe.com.tranzact.challenge.automation.driver.DriverFactory;
import pe.com.tranzact.challenge.automation.dto.AddressInfo;
import pe.com.tranzact.challenge.automation.dto.ItemDetail;
import pe.com.tranzact.challenge.automation.pom.card.ShopItemCard;
import pe.com.tranzact.challenge.automation.pom.page.*;

import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Slf4j
@Tag("web")
public class ChallengeWebTest {

    private WebDriver driver;

    static Stream<Arguments> users(){
        log.info("start setup ");
        List<Arguments> testData = new ArrayList<>();
        IntStream.range(0,1).forEach(idx -> {
            String fullName = String.format("%s %s",RandomStringUtils.randomAlphabetic(6),RandomStringUtils.randomAlphabetic(6));
            log.info(fullName);

            testData.add(Arguments.arguments(
                    fullName,
                    String.format("%s@%s.com", RandomStringUtils.randomAlphabetic(6), RandomStringUtils.randomAlphanumeric(10)),
                    RandomStringUtils.randomAlphanumeric(10),
                    new AddressInfo(
                            fullName,
                            String.valueOf(RandomUtils.nextInt(1000000, 9999999)),//RandomStringUtils.random(1,1000000,9999999,false,true),
                            RandomStringUtils.randomAlphanumeric(20),
                            RandomStringUtils.randomAlphanumeric(10),
                            "United States",
                            "Alabama",
                            String.valueOf(RandomUtils.nextInt(10000, 99999))
                            )
                    )
            );
        });
        log.info("returning the elements");
        return testData.stream();
    }

    @BeforeEach
    public void setup(){

        Optional<String> navigator = Optional.ofNullable(System.getenv("navigator")).filter(Predicate.not(String::isEmpty));

        driver = new DriverFactory().getDriver(navigator.orElse(DriverFactory.CHROME));
        driver.get(PropertiesSingleton.getInstance().getProperty("url.web"));
    }

    @AfterEach
    public void tearDown(){
        driver.quit();
    }


    @ParameterizedTest
    @MethodSource("users")
    public void test01(String fullName, String email, String password,AddressInfo addressInfo) {
        log.info("Full name: {}",fullName);
        log.info("Email: {}",email);
        log.info("Password: {}",password);
        AtomicReference<HomePage> home = new AtomicReference<>(new HomePage(driver)
                .menu()
                .goLoginPage()
                .createAccount()
                .fillDataToRegister(fullName, email, password));

        List<ItemDetail> items = new ArrayList<>();

        IntStream.range(0,3).forEach(value -> {
            log.info("iteration: {}",value);
            ShopPage shop = home.get()
                    .cardItems()
                    .stream()
                    .findAny()
                    .orElseThrow(() -> new RuntimeException("No available content"))
                    .goToShop();

            ShopItemCard card = shop.getDisplayedShopItems().get(value);
            ShopItemDetailPage detail = card.viewItem();
            detail.fillAnySize()
                    .fillAnyColor()
                    .fillQuantity(RandomUtils.nextInt(1, 10))
                    .addToCart();

            items.add(detail.getDetailInformation());

            home.set(detail.menu().goHome());

        });

        CartPage page = home.get().menu().goToShoppingCart();

        Assertions.assertEquals(
                items.stream().sorted((o1, o2) -> o1.getName().compareToIgnoreCase(o2.getName())).collect(Collectors.toList()),
                page.tableCard().getItemDetail().stream().sorted((o1, o2) -> o1.getName().compareToIgnoreCase(o2.getName())).collect(Collectors.toList()));

        SuccessPage successPage = page.checkout()
                .getShippingAddressPage()
                .fillShippingInformation(addressInfo)
                .continuePayment()
                .selectFirstPaymentOption()
                .placeOrder();

        Assertions.assertEquals(fullName,successPage.getContractInformation());
        Assertions.assertEquals("Cash On Delivery",successPage.getPaymentMethod());
        Assertions.assertEquals(addressInfo,successPage.getShippingAddress());

    }

}
