package pe.com.tranzact.challenge.automation;

import com.google.gson.JsonObject;
import io.restassured.http.ContentType;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.stream.Stream;

import static io.restassured.RestAssured.*;
import static io.restassured.matcher.RestAssuredMatchers.*;
import static org.hamcrest.Matchers.*;
import static io.restassured.module.jsv.JsonSchemaValidator.*;


@Tag("api")
public class ChallengeApiTest {

    static Stream<Arguments> country(){
        return Stream.of(
                Arguments.arguments("Test Country","TC","TCY")
        );
    }

    @Test
    public void validateSuccessAll(){

        when().get(PropertiesSingleton.getInstance().getProperty("api.get.all"))
                .then().statusCode(HttpStatus.SC_OK);
        //we have to wait 1 seconds before to run again. It's free plan
        try {
            Thread.sleep(1000*2);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    @ParameterizedTest
    @ValueSource(strings = {"US","DE","GB"})
    public void validateEachCountry(String country){
        when().get(PropertiesSingleton.getInstance().getProperty("api.get.specific"),country)
                .then().statusCode(HttpStatus.SC_OK)
                .assertThat().body(matchesJsonSchemaInClasspath("country-schema.json"));
        //we have to wait 1 seconds before to run again. It's free plan
        try {
            Thread.sleep(1000*2);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

    }

    @ParameterizedTest
    @ValueSource(strings = {"XX","NULL"})
    public void invalidateEachCountry(String country){
        when().get(PropertiesSingleton.getInstance().getProperty("api.get.specific"),country)
                .then().statusCode(HttpStatus.SC_NOT_FOUND).body("status",equalTo(HttpStatus.SC_NOT_FOUND),"message",equalTo("Not Found"));
        //we have to wait 1 second before to run again. It's free plan
        try {
            Thread.sleep(1000*2);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

    }
    @ParameterizedTest
    @MethodSource("country")
    @Disabled("Post for example")
    public void postNewCountry(String name, String alpha2, String alpha3){
        //TODO: as it is a simple object I don't prefer to use jackson to marshall the object.
        JsonObject requestParams = new JsonObject();
        requestParams.addProperty("name", name);
        requestParams.addProperty("alpha2_code", alpha2);
        requestParams.addProperty("alpha3_code", alpha3);

        given().contentType(ContentType.JSON)
                .body(requestParams.toString())
                .when().post(PropertiesSingleton.getInstance().getProperty("api.post"))
                .then().statusCode(HttpStatus.SC_NO_CONTENT);
    }



}
