package pe.com.tranzact.challenge.automation.pom.card;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import pe.com.tranzact.challenge.automation.dto.ItemDetail;
import pe.com.tranzact.challenge.automation.pom.BaseModel;

import java.util.List;
import java.util.stream.Collectors;

public class SummaryTableCard extends BaseModel {

    private WebElement container;
    public SummaryTableCard(WebDriver driver, WebElement container) {
        super(driver);
        this.container = container;
    }

    public List<ItemDetail> getItemDetail() {
        return container.findElements(By.tagName("tr")).stream()
                .map(we ->
                        new ItemDetail(
                                getText(we.findElement(By.cssSelector("td > div > div.cart-tem-info > a"))).toUpperCase(),
                                getText(we.findElement(By.xpath(".//div/div[@class='cart-tem-info']/div/ul/li/span[text()='Size']/following-sibling::span"))),
                                getText(we.findElement(By.xpath(".//div/div[@class='cart-tem-info']/div/ul/li/span[text()='Color']/following-sibling::span"))),
                                Integer.parseInt(getText(we.findElement(By.cssSelector("td >span:first-child"))))) ).collect(Collectors.toList());

    }
}
