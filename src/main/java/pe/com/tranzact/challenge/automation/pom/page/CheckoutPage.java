package pe.com.tranzact.challenge.automation.pom.page;

import org.openqa.selenium.WebDriver;
import pe.com.tranzact.challenge.automation.pom.BaseModel;

public class CheckoutPage extends BaseModel {

    private ShippingAddressPage shippingAddressPage;
    public CheckoutPage(WebDriver driver) {
        super(driver);
        this.shippingAddressPage = new ShippingAddressPage(driver);
    }

    public ShippingAddressPage getShippingAddressPage() {
        return shippingAddressPage;
    }
}
