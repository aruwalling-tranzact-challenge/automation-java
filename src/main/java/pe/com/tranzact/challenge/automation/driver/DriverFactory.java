package pe.com.tranzact.challenge.automation.driver;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chromium.ChromiumDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

public class DriverFactory {
    public final static String FIREFOX = "firefox";
    public final static String CHROME = "chrome";


    private final Map<String, Supplier<WebDriver>> mapDrivers;

    public DriverFactory() {
        mapDrivers = new HashMap<>();

        mapDrivers.put(FIREFOX,() -> {
            WebDriverManager.firefoxdriver().setup();
            return new FirefoxDriver();
        });
        mapDrivers.put(CHROME,() -> {
            WebDriverManager.chromedriver().setup();
            return new ChromeDriver();
        });
    }

    public WebDriver getDriver(String driverName){
        if(!mapDrivers.containsKey(driverName)){
            throw new IllegalArgumentException(String.format("No such driver %s",driverName));
        }
        return mapDrivers.get(driverName).get();
    }
}
