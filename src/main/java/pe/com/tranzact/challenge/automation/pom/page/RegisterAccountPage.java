package pe.com.tranzact.challenge.automation.pom.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pe.com.tranzact.challenge.automation.pom.BaseModel;

public class RegisterAccountPage  extends BaseModel {

    @FindBy(css = "input[name='full_name']")
    private WebElement fullNameInput;
    @FindBy(css = "input[name='email']")
    private WebElement usernameInput;

    @FindBy(css = "input[name='password']")
    private WebElement passwordInput;
    @FindBy(css = "a.text-interactive")
    private WebElement loginLink;

    @FindBy(css = "button.button")
    private WebElement signInButton;

    public RegisterAccountPage(WebDriver driver) {
        super(driver);
    }

    public LoginPage goToLogin(){
        click(loginLink);
        return new LoginPage(driver);
    }

    public HomePage fillDataToRegister(String fullName, String email, String password){
        type(fullName,fullNameInput);
        type(email,usernameInput);
        type(password,passwordInput);
        click(signInButton);
        return new HomePage(driver);
    }



}
