package pe.com.tranzact.challenge.automation.pom.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pe.com.tranzact.challenge.automation.pom.BaseModel;

import java.util.List;

public class PaymentPage  extends BaseModel {

    @FindBy(css = ".py-2 > div>div > div > a")
    private List<WebElement> paymentsOptions;

    @FindBy(css = "button.button")
    private WebElement placeOrder;




    public PaymentPage(WebDriver driver) {
        super(driver);
    }

    public PaymentPage selectFirstPaymentOption(){
        waitUntilElementExist(By.cssSelector(".py-2 > div>div > div > a"));
        click(paymentsOptions.stream().findFirst().orElseThrow(() -> new RuntimeException("No payment method available")));
        return this;
    }

    public SuccessPage placeOrder(){
        waitUtilElementIsVisible(placeOrder);
        click(placeOrder);
        return new SuccessPage(driver);
    }

}
