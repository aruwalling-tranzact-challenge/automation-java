package pe.com.tranzact.challenge.automation.pom.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pe.com.tranzact.challenge.automation.dto.AddressInfo;
import pe.com.tranzact.challenge.automation.pom.BaseModel;

public class SuccessPage extends BaseModel {

    @FindBy(xpath = "//div/h3[text()='Contact information']/parent::div/following-sibling::div")
    private WebElement contactInformation;

    @FindBy(xpath = "//div/h3[text()='Payment Method']/parent::div/following-sibling::div")
    private WebElement paymentMethod;

    @FindBy(css = "div.full-name")
    private WebElement fullName;

    @FindBy(css = "div.address-one")
    private WebElement addressOne;

    @FindBy(css = "div.city-province-postcode > div:first-child")
    private WebElement postcodeCity;

    @FindBy(css = "div.city-province-postcode > div:last-child")
    private WebElement provinceCountry;

    @FindBy(css = "div.telephone")
    private WebElement telephone;


    public SuccessPage(WebDriver driver) {
        super(driver);
        waitUntilElementExist(By.cssSelector("div.full-name"));
    }


    public String getContractInformation(){
        return getText(contactInformation);
    }

    public String getPaymentMethod(){
        return  getText((paymentMethod));
    }

    public AddressInfo getShippingAddress(){
        return new AddressInfo(
                getText(fullName),
                getText(telephone),
                getText(addressOne),
                getText(postcodeCity).split(",")[1].trim(),
                getText(provinceCountry).split(",")[1].trim(),
                getText(provinceCountry).split(",")[0].trim(),
                getText(postcodeCity).split(",")[0].trim()
        );
    }

}
