package pe.com.tranzact.challenge.automation.pom.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import pe.com.tranzact.challenge.automation.pom.BaseModel;
import pe.com.tranzact.challenge.automation.pom.card.ShopItemCard;

import java.util.List;
import java.util.stream.Collectors;

public class ShopPage  extends BaseModel {

    @FindBy(css = ".listing-tem")
    private List<WebElement> elements;

    public ShopPage(WebDriver driver) {
        super(driver);
    }

    public MenuPage menu(){
        return new MenuPage(driver);
    }

    public List<ShopItemCard> getDisplayedShopItems(){
        return elements.stream().map(webElement -> new ShopItemCard(driver,webElement)).collect(Collectors.toList());
    }





}
