package pe.com.tranzact.challenge.automation.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class AddressInfo {

    private String fullName;
    private String telephone;
    private String addressLine;
    private String city;
    private String country;
    private String province;
    private String postcode;

}
