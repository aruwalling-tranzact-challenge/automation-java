package pe.com.tranzact.challenge.automation.pom.page;

import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pe.com.tranzact.challenge.automation.dto.ItemDetail;
import pe.com.tranzact.challenge.automation.pom.BaseModel;


@Slf4j
public class ShopItemDetailPage extends BaseModel {


    @FindBy(css = "input[name='qty']")
    private WebElement qtyInput;

    @FindBy(css = "h1")
    private WebElement nameTitle;

    @FindBy(css = "button")
    private WebElement addToCartButton;

    @FindBy(xpath = "//div/input[@type='hidden']/following::ul[1]")
    private WebElement sizeList;

    @FindBy(xpath = "//div/input[@type='hidden']/following::ul[2]")
    private WebElement colorList;

    private WebElement selectedSize;
    private WebElement selectedColor;

    private String price;

    public ShopItemDetailPage(WebDriver driver) {
        super(driver);
    }

    public MenuPage menu(){
        return new MenuPage(driver);
    }

    public ShopItemDetailPage fillQuantity(int quantity){
        price=String.valueOf(quantity);
        qtyInput.clear();
        type(price,qtyInput);
        return this;
    }

    public ShopItemDetailPage fillAnySize(){
        selectedSize=sizeList.findElements(By.cssSelector("a")).stream().findAny().get();
        click(selectedSize);
        waitUntilNumberOfElementsISPresent(By.cssSelector("li.selected"),1);
        return this;
    }

    public ShopItemDetailPage fillAnyColor(){
        selectedColor=colorList.findElements(By.cssSelector("a")).stream().findAny().get();
        click(selectedColor);
        waitUntilNumberOfElementsISPresent(By.cssSelector("li.selected"),2);
        return this;
    }

    public ItemDetail getDetailInformation(){
        return new ItemDetail(getText(nameTitle).toUpperCase(),getText(selectedSize),getText(selectedColor),Integer.parseInt(price));
    }

    public ShopItemDetailPage addToCart(){
        waitUntilNumberOfElementsISPresent(By.cssSelector("li.selected"),2);
        click(addToCartButton);
        return this;
    }

}
