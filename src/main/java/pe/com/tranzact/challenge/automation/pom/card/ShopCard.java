package pe.com.tranzact.challenge.automation.pom.card;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import pe.com.tranzact.challenge.automation.pom.BaseModel;
import pe.com.tranzact.challenge.automation.pom.page.ShopPage;

public class ShopCard  extends BaseModel {

    private WebElement container;

    private WebElement buttonShop;

    public ShopCard(WebDriver driver, WebElement container) {
        super(driver);
        this.container=container;
        buttonShop = this.container.findElement(By.tagName("a"));
    }

    public String getShopDescription(){
        return buttonShop.getText();
    }

    public ShopPage goToShop(){
        click(buttonShop);
        return new ShopPage(driver);
    }

}
