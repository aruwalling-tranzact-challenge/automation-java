package pe.com.tranzact.challenge.automation;

import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.Properties;

@Slf4j
public class PropertiesSingleton {

    private static PropertiesSingleton instance =null;
    private Properties properties;

    private PropertiesSingleton() {

        properties= new Properties();
        try {
            properties.load(PropertiesSingleton.class.getClassLoader().getResourceAsStream("path.properties"));
        } catch (IOException e) {
            log.error("",e);
        }
    }

    public static PropertiesSingleton getInstance() {

        if (instance ==null) {

            instance =new PropertiesSingleton();
        }
        return instance;
    }

    public String getProperty(String key) {

        return properties.getProperty(key);
    }


}
