package pe.com.tranzact.challenge.automation.pom.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import pe.com.tranzact.challenge.automation.pom.BaseModel;

public class LoginPage extends BaseModel {

    @FindBy(css = "input[name='email']")
    private WebElement usernameInput;

    @FindBy(css = "input[name='password']")
    private WebElement passwordInput;

    @FindBy(css = "button.button")
    private WebElement signInButton;

    @FindBy(xpath = "//a[@class='text-interactive' and contains(text(),'Create an account')]")
    private WebElement createAccount;

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public RegisterAccountPage createAccount(){
        click(createAccount);
        return new RegisterAccountPage(driver);
    }


}
