package pe.com.tranzact.challenge.automation.pom.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;
import pe.com.tranzact.challenge.automation.dto.AddressInfo;
import pe.com.tranzact.challenge.automation.pom.BaseModel;

public class ShippingAddressPage extends BaseModel {
    @FindBy(css = "input[name='address[full_name]']")
    private WebElement fullNameInput;

    @FindBy(css = "input[name='address[telephone]']")
    private WebElement telephoneInput;

    @FindBy(css = "input[name='address[address_1]']")
    private WebElement addressInput;

    @FindBy(css = "input[name='address[city]']")
    private WebElement cityInput;

    @FindBy(css = "input[name='address[postcode]']")
    private WebElement postcodeInput;

    @FindBy(css = "select[name='address[country]']")
    private WebElement countrySelect;

    @FindBy(css = "select[name='address[province]']")
    private WebElement provinceSelect;

    @FindBy(id="method0")
    private WebElement freeShippingRadio;

    @FindBy(css = "button.button")
    private WebElement goToPaymentButton;

    public ShippingAddressPage(WebDriver driver) {
        super(driver);
        waitUntilNumberOfElementsISPresent(By.cssSelector("select[name='address[country]']"),1);
    }

    public ShippingAddressPage fillShippingInformation(AddressInfo info){

        type(info.getFullName(),fullNameInput);
        type(info.getTelephone(),telephoneInput);
        type(info.getAddressLine(),addressInput);
        type(info.getCity(),cityInput);
        this.selectCountry(info.getCountry()).selectProvince(info.getProvince());
        type(info.getPostcode(),postcodeInput);
        init();
        return this;
    }

    public ShippingAddressPage selectCountry(String country){
        Select drpCountry = new Select(countrySelect);
        drpCountry.selectByVisibleText(country);
        return this;
    }

    public ShippingAddressPage selectProvince(String province){
        Select drpProvince = new Select(provinceSelect);
        drpProvince.selectByVisibleText(province);
        return this;
    }

    public PaymentPage continuePayment(){
        waitUntilElementExist(By.id("method0"));
        goAndClick(freeShippingRadio);
        click(goToPaymentButton);
        return new PaymentPage(driver);
    }


}
