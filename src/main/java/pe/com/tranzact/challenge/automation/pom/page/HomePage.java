package pe.com.tranzact.challenge.automation.pom.page;

import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pe.com.tranzact.challenge.automation.pom.BaseModel;
import pe.com.tranzact.challenge.automation.pom.card.ShopCard;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
public class HomePage extends BaseModel {


    @FindBy(css = ".mt-15 > div >div")
    private List<WebElement> shopItems;

    public HomePage(WebDriver driver) {
        super(driver);
        waitUntilNumberOfElementsISPresent(By.cssSelector(".mt-15 > div >div"),3);

    }

    public MenuPage menu(){
        return  new MenuPage(driver);
    }

    public List<ShopCard> cardItems(){
        log.info("items: {}",shopItems);
        return shopItems.stream().map(webElement -> new ShopCard(driver,webElement)).collect(Collectors.toList());
    }



}
