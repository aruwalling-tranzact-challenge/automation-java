package pe.com.tranzact.challenge.automation.dto;


import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ItemDetail {
    private String name;
    private String size;
    private String color;
    private Integer quantity;
}
