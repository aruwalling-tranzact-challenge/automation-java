package pe.com.tranzact.challenge.automation.pom.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pe.com.tranzact.challenge.automation.pom.BaseModel;

public class MenuPage extends BaseModel {

    @FindBy(css = ".logo-icon")
    private WebElement homeLink;
    @FindBy(css = ".mini-cart-icon")
    private WebElement cartLink;
    @FindBy(xpath = "//li [@class='nav-item']/ a[text()='Men']")
    private WebElement menLink;
    @FindBy(xpath = "//li [@class='nav-item']/ a[text()='Women']")
    private WebElement womenLink;
    @FindBy(xpath = "//li [@class='nav-item']/ a[text()='Kids']")
    private WebElement kidsLink;
    @FindBy(xpath = "//div/div[@class='self-center']/a")
    private WebElement signInLink;
    public MenuPage(WebDriver driver) {
        super(driver);
    }

    public LoginPage goLoginPage(){
        click(signInLink);
        return new LoginPage(driver);
    }

    public HomePage goHome(){
        click(homeLink);
        return new HomePage(driver);
    }
    public CartPage goToShoppingCart(){
        click(cartLink);
        return new CartPage(driver);
    }
}
