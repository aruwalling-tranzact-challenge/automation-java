package pe.com.tranzact.challenge.automation.pom;

import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

import java.time.Duration;
import java.util.List;

@Slf4j
public class BaseModel {
    protected WebDriver driver;

    public BaseModel(WebDriver driver) {
        this.driver = driver;
        this.init();

    }

    protected void init(){
        PageFactory.initElements(this.driver, this);
    }

    public WebElement findElement(By locator) {
        return driver.findElement(locator);
    }

    public List<WebElement> findElements(By locator) {
        return driver.findElements(locator);
    }

    public String getText(WebElement element) {
        return element.getText();
    }

    public String getText(By locator) {
        return driver.findElement(locator).getText();
    }

    public void type(String inputText, By locator) {
        driver.findElement(locator).sendKeys(inputText);
    }

    public void type(String inputText, WebElement element) {
        element.sendKeys(inputText);
    }

    public void click(By locator) {
        driver.findElement(locator).click();
    }

    public void click(WebElement element) {
        element.click();
    }

    public Boolean isDisplayed(By locator) {
        try {
            return driver.findElement(locator).isDisplayed();
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    public void goAndClick(WebElement element){
        new Actions(driver).moveToElement(element).click().build().perform();
    }

    public void visit(String url) {
        driver.get(url);
    }

    public String getTitle() {
        return driver.getTitle();
    }

    public void waitUntilElementIsClickable(By locator) {
        Wait<WebDriver> fWait = new FluentWait<>(driver).withTimeout(Duration.ofSeconds(60))
                .pollingEvery(Duration.ofSeconds(2)).ignoring(StaleElementReferenceException.class).ignoring(NoSuchElementException.class);
        fWait.until(ExpectedConditions.elementToBeClickable(locator));
    }

    public void waitUntilElementExist(By locator) {
        Wait<WebDriver> fWait = new FluentWait<>(driver).withTimeout(Duration.ofSeconds(60))
                .pollingEvery(Duration.ofSeconds(2)).ignoring(StaleElementReferenceException.class)
        .ignoring(NoSuchElementException.class);
        fWait.until(webDriver -> webDriver.findElement(locator));
    }
    public void acceptAlert(){
        driver.switchTo().alert().accept();
    }
    public void dismissAlert(){
        driver.switchTo().alert().dismiss();
    }

    public void waitUntilNumberOfElementsISPresent(By locator, int nElements){
        new FluentWait(driver).withTimeout(Duration.ofSeconds(40))
                .pollingEvery(Duration.ofSeconds(5))
                .ignoring(java.util.NoSuchElementException.class)
                .until(ExpectedConditions.numberOfElementsToBe(locator,nElements));
    }

    public void waitUtilElementIsVisible(WebElement element){
        new FluentWait(driver).withTimeout(Duration.ofSeconds(120))
                .pollingEvery(Duration.ofSeconds(5))
                .ignoring(java.util.NoSuchElementException.class)
                .until(ExpectedConditions.visibilityOf(element));
    }



}
