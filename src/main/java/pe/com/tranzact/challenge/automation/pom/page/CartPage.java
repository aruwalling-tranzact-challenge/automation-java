package pe.com.tranzact.challenge.automation.pom.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pe.com.tranzact.challenge.automation.pom.BaseModel;
import pe.com.tranzact.challenge.automation.pom.card.SummaryTableCard;

public class CartPage extends BaseModel {

    @FindBy(css = "tbody")
    private WebElement tableBodyContainer;
    @FindBy(css = "a.button")
    private WebElement checkoutButton;

    public CartPage(WebDriver driver) {
        super(driver);
    }

    public SummaryTableCard tableCard(){
        return new SummaryTableCard(driver,tableBodyContainer);
    }

    public MenuPage manu(){
        return new MenuPage(driver);
    }

    public CheckoutPage checkout(){
        click(checkoutButton);
        return new CheckoutPage(driver);
    }

}
