package pe.com.tranzact.challenge.automation.pom.card;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import pe.com.tranzact.challenge.automation.pom.BaseModel;
import pe.com.tranzact.challenge.automation.pom.page.ShopItemDetailPage;

public class ShopItemCard extends BaseModel {

    private WebElement container;

    private WebElement itemName;

    private WebElement itemPrice;

    private WebElement linkElement;

    public ShopItemCard(WebDriver driver, WebElement container) {
        super(driver);
        this.container = container;
        itemName =this.container.findElement(By.cssSelector(" a > span"));
        itemPrice =this.container.findElement(By.cssSelector(" div > span"));
        linkElement =this.container.findElement(By.xpath(".//a/img/parent::a"));
    }

    public String getName(){
        return itemName.getText();
    }
    public String getPrice(){
        return itemPrice.getText();
    }

    public ShopItemDetailPage viewItem(){
        click(linkElement);
        return new ShopItemDetailPage(driver);
    }


}
