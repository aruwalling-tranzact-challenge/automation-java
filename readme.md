Tranzact Challenge
==============================================================

_Project developed  using https://demo.evershop.io/ for Selenium and https://api.countrylayer.com/v2/alpha for API Testing_

## Getting Starting 🚀

_This instruction let you obtain a project copy to execute tests_

### Prerequisite 📋

_Basic tools you need to execute_

* JDK 11 
* Maven
* Intellij Idea Community (Optional) 


_You can install [SDK Man](https://sdkman.io/) to setup easily Java and Maven or another Java Base Tool_

### Installation 🔧

_I recommend you to use Intellij Idea. All tests are at  pe.com.tranzact.challenge.automation package._

_To execute in the command line, maven helps us with the followings tag:_

* webDriver
* web
* api

```
mvn verify -Dtests=<tag>
```

_The following navigators were implemented if you want to change navigator_

* firefox
* chrome

```
export navigator=<navigator>
mvn verify -Dtests=<tag>
```

_**Note: webDriver requires Chrome and Firefox an by default is chrome**_

## Libraries 🛠️

_This project was developed with the followings libraries:_

* [Selenium](https://www.selenium.dev/) - Help us to control the navigator
* [slf4j](https://www.slf4j.org/) - Help us to manage the log. It Supports All the Main Logging Frameworks
* [WebDriverManager](https://bonigarcia.dev/webdrivermanager/) - Help us to download the most important web driver implementations such as Chrome, Firefox and so on.
* [Lombok](https://projectlombok.org/) - Help us to generate code through annotations such as getter, setter, constructor, log and so on.
* [Junit 5](https://junit.org/junit5/) - This is the runner used. This version it quite simple then TestNG and many configuration to introduce data.
* [Apache Commons Lang](https://commons.apache.org/proper/commons-lang/) Help us to generate random number and alphabetic string sequence.
* [Rest-assured](https://rest-assured.io/) - Help us to call apis in a functional way. Also we use json path and json-schema-validation to get information and validate structure from a json

## Author ✒️
* **Antonio Romain Ugaz Walling** - *Developer* - [LinkedIn](https://www.linkedin.com/in/antonio-romain-ugaz-walling-76294020/?originalSubdomain=pe)













